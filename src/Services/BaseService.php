<?php

namespace Ktnw\CurdSupport\Services;

use Ktnw\CurdSupport\Params\AbstractBaseParams;
use Ktnw\CurdSupport\Wrappers\QueryResultVo;
use Ktnw\CurdSupport\Wrappers\QueryWrapperParams;
use Ktnw\CurdSupport\Wrappers\SaveOrUpdateWrapper;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * 基础service接口
 */
interface BaseService
{
    /**
     * 查询model或实例
     * @param $primary int 主键
     * @param $modelClass string model的class
     * @param bool $isInstance 是否返回实例 true-是；false-否; 为false时，返回array数组.
     * @return Model|array
     * @throws Exception
     */
    public function findOne(int $primary, string $modelClass, bool $isInstance = true);

    /**
     * 查询model或实例
     * @param $where array 条件
     * @param $modelClass string model的class
     * @param bool $isInstance 是否返回实例 true-是；false-否; 为false时，返回array数组.
     * @return Model|array
     * @throws Exception
     */
    public function findOneByKeys(array $where, string $modelClass, bool $isInstance = true);

    /**
     * delete实例
     * @param $primary int 主键
     * @param $modelClass string model的class
     * @param bool $pseudoDeletion 是否伪删除 true-是；false-否; true时做逻辑删除;false时，做物理删除.
     * @return bool
     * @throws Exception
     */
    public function delete(int $primary, string $modelClass, bool $pseudoDeletion = false): bool;

    /**
     * 保存实例
     * @param AbstractBaseParams $params object|array 封装实例的参数
     * @param $modelClass string model的class
     * @param SaveOrUpdateWrapper|null $mappers SaveOrUpdateWrapper
     * @param $ignoreColumn array 要忽略的字段
     * @return bool
     * @throws Exception
     */
    public function saveOrUpdate(AbstractBaseParams $params, string $modelClass, SaveOrUpdateWrapper $mappers = null, array $ignoreColumn = []): bool;

    /**
     * 保存实例
     * @param AbstractBaseParams $params object|array 封装实例的参数
     * @param $modelClass string model的class
     * @param SaveOrUpdateWrapper|null $mappers SaveOrUpdateWrapper
     * @param $ignoreColumn array 要忽略的字段
     * @return int 实例id
     * @throws Exception
     */
    public function saveOrMerge(AbstractBaseParams $params, string $modelClass, SaveOrUpdateWrapper $mappers = null, array $ignoreColumn = []): int;

    /**
     * 获取列表
     * @param $wrapperParams QueryWrapperParams
     * @return QueryResultVo
     */
    public function findList(QueryWrapperParams $wrapperParams): QueryResultVo;

    /**
     * 使用ORM model 查询
     * @param string $modelClass
     * @param array $where 查询条件
     * @param array $orWhere
     * @param array $findColumn
     * @param array $inWhere
     * @param array $orderBy 排序
     * @param bool $singleton 是否查询单条数据
     * @return array
     * @throws Exception
     */
    public function modelSelect(string $modelClass, array $where, array $orWhere = [], array $findColumn = [], array $inWhere = [], array $orderBy = [], bool $singleton = false): array;


    /**
     * 使用DB::select查询
     * @param $sql string 查询sql
     * @param $params array sql参数
     * @param $isSingle boolean 是否返回一条记录 true-是; false-否;
     * @return array
     */
    public function dbSelect(string $sql, array $params, bool $isSingle = false): array;

    /**
     * 通用的修改数据库方法
     * @param string $sql
     * @param array $params
     * @return bool
     */
    public function dbUpdate(string $sql, array $params): bool;


    /**
     * 删除
     * @param $sql
     * @param $params
     * @return bool
     */
    public function dbDelete($sql, $params): bool;


}