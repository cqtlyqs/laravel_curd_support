<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * 封装的查询条件
 * Date: 2022/2/9
 * Time: 9:09
 */

namespace Ktnw\CurdSupport\Wrappers;


class QueryWrapperParams
{
    // 查询类型 1-通过model查询. 2-通过DB::select()查询. 默认为1.
    public $queryType;

    // model查询需要的查询条件及参数
    // $where 、 $inWhere 、$orWhere 为and关系. $orWhere中的为 OR 关系.
    private $where; // array
    private $orWhere; // array
    private $inWhere; // array
    private $notInWhere; // array
    private $findColumn; // array
    private $orderBy; // array
    private $modelClass;
    private $usePresenter; // bool

    // DB::select()查询需要的参数
    private $totalSql;
    private $sql;
    private $params;

    // 两者都需要
    private $page;
    private $size;

    /**
     * QueryWrapperParams constructor.
     * @param $queryType
     */
    public function __construct($queryType)
    {
        $this->queryType    = $queryType;
        $this->where        = [];
        $this->orWhere      = [];
        $this->inWhere      = [];
        $this->notInWhere   = [];
        $this->findColumn   = [];
        $this->orderBy      = [];
        $this->page         = 1;
        $this->size         = 20;
        $this->sql          = "";
        $this->params       = [];
        $this->usePresenter = true;
    }

    /**
     * @return int
     */
    public function getQueryType(): int
    {
        return $this->queryType;
    }

    /**
     * @param mixed $queryType
     */
    public function setQueryType($queryType)
    : void
    {
        $this->queryType = $queryType;
    }

    /**
     * @return array
     */
    public function getWhere()
    : array
    {
        return $this->where;
    }

    /**
     * @param array $where
     */
    public function setWhere(array $where)
    : void
    {
        $this->where = $where;
    }

    /**
     * @return array
     */
    public function getOrWhere()
    : array
    {
        return $this->orWhere;
    }

    /**
     * @param array $orWhere
     */
    public function setOrWhere(array $orWhere)
    : void
    {
        $this->orWhere = $orWhere;
    }

    /**
     * @return array
     */
    public function getInWhere()
    : array
    {
        return $this->inWhere;
    }

    /**
     * @param array $inWhere
     */
    public function setInWhere(array $inWhere)
    : void
    {
        $this->inWhere = $inWhere;
    }

    /**
     * @return array
     */
    public function getFindColumn()
    : array
    {
        return $this->findColumn;
    }

    /**
     * @param array $findColumn
     */
    public function setFindColumn(array $findColumn)
    : void
    {
        $this->findColumn = $findColumn;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    : array
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy(array $orderBy)
    : void
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return int
     */
    public function getPage()
    : int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page)
    : void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getSize()
    : int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size)
    : void
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getSql()
    : string
    {
        return $this->sql;
    }

    /**
     * @param string $sql
     */
    public function setSql(string $sql)
    : void
    {
        $this->sql = $sql;
    }

    /**
     * @return array
     */
    public function getParams()
    : array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    : void
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getTotalSql()
    {
        return $this->totalSql;
    }

    /**
     * @param mixed $totalSql
     */
    public function setTotalSql($totalSql)
    : void
    {
        $this->totalSql = $totalSql;
    }

    /**
     * @return mixed
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @param mixed $modelClass
     */
    public function setModelClass($modelClass)
    : void
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @return array
     */
    public function getNotInWhere()
    : array
    {
        return $this->notInWhere;
    }

    /**
     * @param array $notInWhere
     */
    public function setNotInWhere(array $notInWhere)
    : void
    {
        $this->notInWhere = $notInWhere;
    }

    /**
     * @return bool
     */
    public function getUsePresenter()
    : bool
    {
        return $this->usePresenter;
    }

    /**
     * @param bool $usePresenter
     */
    public function setUsePresenter(bool $usePresenter)
    : void
    {
        $this->usePresenter = $usePresenter;
    }


}