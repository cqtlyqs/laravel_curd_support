<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * 分页查询结果的vo
 * Date: 2021/4/1
 * Time: 8:36
 */

namespace Ktnw\CurdSupport\Wrappers;


class QueryResultVo
{
    public $pageInfo; // 分页信息
    public $dataList; // 数据列表

    /**
     * QueryResultVo constructor.
     * @param PageInfo $pageInfo
     * @param $dataList
     */
    public function __construct(PageInfo $pageInfo, $dataList)
    {
        $this->pageInfo = $pageInfo;
        $this->dataList = $dataList;
    }

    /**
     * @return PageInfo
     */
    public function getPageInfo(): PageInfo
    {
        return $this->pageInfo;
    }

    /**
     * @param mixed $pageInfo
     */
    public function setPageInfo($pageInfo)
    : void
    {
        $this->pageInfo = $pageInfo;
    }

    /**
     * @return mixed
     */
    public function getDataList()
    {
        return $this->dataList;
    }

    /**
     * @param mixed $dataList
     */
    public function setDataList($dataList)
    : void
    {
        $this->dataList = $dataList;
    }


}