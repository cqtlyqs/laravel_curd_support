<?php

namespace Ktnw\CurdSupport\Wrappers;
/**
 * 查询相关常量类
 */
class QueryConstants
{
    /**
     * 使用ORM查询
     */
    const DB_MODEL = 1;

    /**
     * 使用DB:select查询
     */
    const DB_QUERY = 2;

}