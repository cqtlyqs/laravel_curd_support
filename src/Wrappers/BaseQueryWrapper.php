<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * query wrapper 的基类
 * Date: 2022/2/10
 * Time: 9:00
 */

namespace Ktnw\CurdSupport\Wrappers;

abstract class BaseQueryWrapper
{
    // 查询类型 1-通过model查询. 2-通过DB::select()查询. 默认为1.
    public $queryType;
    public $query;
    public $modelClass;

    /**
     * BaseQueryWrapper constructor.
     * @param int $queryType
     * @param BaseQuery|null $query
     * @param string|null $modelClass string model的class
     */
    public function __construct(int $queryType, BaseQuery $query = null, string $modelClass = null)
    {
        $this->queryType  = empty($queryType) ? 1 : $queryType;
        $this->query      = $query;
        $this->modelClass = $modelClass;
    }

    /**
     * 根据查询类型返回查询时需要的查询条件及参数
     * @return QueryWrapperParams
     */
    abstract public function initQueryWrapper(): QueryWrapperParams;
}