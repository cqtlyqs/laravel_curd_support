<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * 分页信息
 * Date: 2022/2/9
 * Time: 8:42
 */

namespace Ktnw\CurdSupport\Wrappers;


class PageInfo
{
    public $page; // 当前页数
    public $size; // 每页查询条数
    public $total; // 查询结果的总条数
    public $totalPage; // 总页数

    /**
     * PageInfo constructor.
     * @param $total int 查询结果的总条数
     * @param $page int 当前页数
     * @param $size int 每页查询条数
     */
    public function __construct(int $total, int $page, int $size)
    {
        $this->setPage($page);
        $this->setSize($size);
        $this->setTotal($total);
        $this->setTotalPage($this->total % $this->size == 0 ? ($this->total / $this->size) : ceil($this->total / $this->size));
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    : void
    {
        $this->page = $page <= 0 ? 1 : $page;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    : void
    {
        $this->size = $size <= 0 ? 1 : $size;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    : void
    {
        $this->total = $total <= 0 ? 0 : $total;
    }

    /**
     * @return mixed
     */
    public function getTotalPage()
    {
        return $this->totalPage;
    }

    /**
     * @param mixed $totalPage
     */
    public function setTotalPage($totalPage)
    : void
    {
        $this->totalPage = intval($totalPage);
    }


}