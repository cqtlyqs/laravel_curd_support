<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * 封装前端查询条件的基类
 * Date: 2021/4/1
 * Time: 8:54
 */

namespace Ktnw\CurdSupport\Wrappers;


class BaseQuery
{
    public $page;
    public $size;
    public $usePresenter;

    /**
     * BaseQuery constructor.
     */
    public function __construct()
    {
        $this->page         = 1;
        $this->size         = 30;
        $this->usePresenter = false;
    }


    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    : void
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize(int $size)
    : void
    {
        $this->size = $size;
    }

    /**
     * @return bool
     */
    public function getUsePresenter()
    : bool
    {
        return $this->usePresenter;
    }

    /**
     * @param bool $usePresenter
     */
    public function setUsePresenter(bool $usePresenter)
    : void
    {
        $this->usePresenter = $usePresenter;
    }


}