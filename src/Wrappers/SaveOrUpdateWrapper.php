<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 *
 * Date: 2022/2/9
 * Time: 15:20
 */

namespace Ktnw\CurdSupport\Wrappers;


use Ktnw\CurdSupport\Params\AbstractBaseParams;

abstract class SaveOrUpdateWrapper
{
    /**
     * 新增或修改model时的校验类 校验通过后 可往下执行。若校验失败抛出Exception异常
     * @param $params AbstractBaseParams
     * @return bool
     */
    abstract function verify(AbstractBaseParams $params): bool;

}