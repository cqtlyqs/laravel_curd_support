<?php

namespace Ktnw\CurdSupport\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

trait BaseModelTrait
{
    /**
     * 返回主键的key
     */
    public function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }


    /**
     * 若存在逻辑删除字段，请自行设置。默认为[]。
     * ["status", 0]
     * status: 逻辑删除标志;
     * 0: 逻辑删除的值。表示status的值为0时，为删除;
     * @return array
     */
    public function pseudoDeletionStatus(): array
    {
        return [];
    }

    /**
     * 逻辑删除
     * @return bool
     */
    public function pseudoDeletion(): bool
    {
        $status = $this->pseudoDeletionStatus();
        if (empty($status)) {
            return false;
        }
        $this->setAttribute($status[0], $status[1]);
        return $this->update();
    }

    /**
     * 查询model
     * @param $primary int 主键
     * @return Model
     */
    public function findOne(int $primary): ?Model
    {
        $instance = $this->find($primary);
        if (!$instance) {
            return null;
        }
        // 判断是否存在逻辑删除字段
        $status = $this->pseudoDeletionStatus();
        if ($status) {
            $delStatus = $instance->getAttribute($status[0]);
            if (isset($delStatus) && $delStatus == $status[1]) {
                $instance = null;
            }
        }
        return $instance;
    }

    /**
     * 获取表中的字段
     * 注意:这里可能存在版本问题. mysql8版本，这里可能获取不到值.
     * @return array
     */
    public function getTableColumns(): array
    {
        return Schema::getColumnListing($this->getTable());
    }

}