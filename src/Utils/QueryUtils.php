<?php

namespace Ktnw\CurdSupport\Utils;

use Illuminate\Support\Str;
use ReflectionClass;

class QueryUtils
{
    /**
     * 根据查询参数动态设置查询条件
     * 业务逻辑: 若查询参数字段与数据库表字段对应，则自动设置为查询条件.
     *
     * @param object $query 查询参数
     * @param string $modelClass
     * @param array $exclude 排除属性
     * @return array
     */
    public static function queryCriteria(object $query, string $modelClass, array $exclude = []): array
    {
        $refQueryClass = new ReflectionClass($query);
        $props         = $refQueryClass->getProperties();
        if (!$props) {
            return [];
        }

        // 获取表结构字段
        $model   = new $modelClass();
        $columns = $model->getTableColumns();
        if (empty($columns)) {
            return [];
        }

        $where = [];
        foreach ($props as $key => $prop) {
            if (!empty($exclude) && in_array($prop->getName(), $exclude)) {
                // 若是需要排除的属性  则跳过本次循环
                continue;
            }
            if (!$prop->isPublic()) {
                $prop->setAccessible(true);
            }
            // 驼峰转为下划线
            $propName = Str::snake($prop->getName());
            $value   = $prop->getValue($query);
            if ($value && in_array($propName, $columns)) {
                // 查询参数与数据库字段匹配
                $where[] = [$propName, '=', $value];
            }
        }
        return $where;
    }
}