<?php
/**
 * Created by PhpStorm.
 * User: youqingsong
 * Date: 2021/3/31
 * Time: 15:07
 */

namespace Ktnw\CurdSupport\Params;


abstract class AbstractBaseParams
{

    /**
     * 获取主键的值
     * @return mixed
     */
    abstract public function getPrimaryValue();

}